<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 9</title>
    <style>
        .piramide{
            font-family: Arial, Helvetica, sans-serif;
            background-color: #08605F;
            color: #A2AD59;
            padding: 2rem;
        }
    </style>
</head>
<body>
    <div class="piramide" style="text-align:center">
        <?php
            for ($i=1; $i <= $_GET['numero']; $i++) { 
                for ($j=1; $j <= $i; $j++) {
                    echo "<span> * </span>";
                }
                echo "<br>";
            }
        ?>
        <h1>Morry Christmas 🎄</h1>
    </div>
</body>
</html>

<!--
Acceder con variable GET - Ej9.php?numero=10
!>

Solucion Alternativa
// /*
//   Recogemos el valor de la longitud primero mirando si se pasa como parametro GET,
//   es decir por la url, como un parámetro extra en la url, ej:
//     http://localhost/solucion.php&longitud=5
//   o bien como un parámetro que se pasa al ejecutar el script desde la consola ej:
//     php solucion_1.php 5
// */
// $longitud = isset($_GET['longitud']) ? $_GET['longitud'] : $argv[1];
// if (empty($longitud)) {
//     die("NO SE HA DEFINIDO LA LONGITUD\n"); //terminamos el script
// }
// /*  La clave del ejercicio es pensar con papel y lápiz qué patrón sigue la generación
//     de la pirámide, si lo pensamos seria algo tal que así para una longitud de 5:
// fila1 ->  hay 4 "espacios en blanco" y luego 1 asterisco
// fila2 ->  hay 3 "espacios en blanco" y luego 1 asterisco, 1 espacio en blanco y otro asterisco
// fila3 ->  hay 2 "espacios en blanco" y luego 1 asterisco, 1 espacio en blanco, 1 asterisco, 1 espacio en blanco y 1 asterisco
// fila3 ->  hay 1 "espacios en blanco" y luego 1 asterisco, 1 espacio en blanco, 1 asterisco, 1 espacio en blanco y 1 asterisco, 1 espacio en blanco y 1 asterisco
// fila3 ->  hay 0 "espacios en blanco" y luego 1 asterisco, 1 espacio en blanco, 1 asterisco, 1 espacio en blanco y 1 asterisco, 1 espacio en blanco y 1 asterisco, 1 espacio en blanco y 1 asterisco
// La primera parte está clara, habrá tantos espacios en blanco como la longitud pasada, menos la fila en la que estamos, es decir
// ej: para la fila 3, el cálculo será 5 - 3
// Y luego lo que hacemos es repetir el patrón "* " tantas veces como en la fila que estemos
// ej: para la fila 3, pintamos: "* " 3 veces, es decir: * * *
// Y por último para no generar un espacio en blanco al final del último asterisco, quitamos los espacios en blanco de la derecha
// http://php.net/manual/es/reserved.variables.argv.php
// http://php.net/manual/es/function.str-repeat.php
// http://php.net/manual/es/function.rtrim.php
// */
// for ($i = 1; $i <= $longitud; $i++) {
//     echo str_repeat(' ', $longitud - $i);
//     echo rtrim(str_repeat('* ', $i))."\n";
// }

Solucion Alternativa 2

if (!empty($_GET['num'])) {
    $num = $_GET['num'];
    $piramide = [];
    for ($i = 0; $i < $num; $i++) {
        $var = "";
        for ($k = 0; $k < $i; $k++) {
            $var = " ".$var;
        }
        $var = $var."*";
        for ($j = 0; $j < $num - $i - 1; $j++) {
            $var = $var." *";
        }
        array_push($piramide, $var);
    }
    echo "<pre>";
    for ($i = count($piramide) - 1; $i >= 0; $i--) {
        echo $piramide[$i]."\n";
    }
    echo "</pre>";
}