<!-- Enunciado: Escribir un programa que dado un número calcule su factorial Es decir, si el número es 5 el resultado sea: 5 x 4 x 3 x 2 x 1 = 120
Objetivo: Comprender las funciones recursivas. -->

<?php

    $num = 8;

    // function factorial(int $num) {
    //     if ($num == 1) {
    //         return 1;
    //     } else {
    //         return $num * factorial($num - 1);
    //     }
    // }
    

    for ($i = $num; $i > 1; $i--) {
        $num *= $i - 1;
    }

    // echo "El factorial de ".$num." es ".factorial($num);
    echo "El factorial es ".$num;