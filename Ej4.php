<?php

    $fichero1 = 'FicheroEjecutable.exe';
    $fichero2 = 'FicheroDeBaseDeDatos.db';

    $total = [$fichero1, $fichero2];

    foreach ($total as $fichero) {
        switch (pathinfo($fichero, PATHINFO_EXTENSION)) {
            case 'exe':
                echo '<p>El fichero '.strtoupper($fichero).' es ejecutable</p>';
                break;
            case 'db':
                echo '<p>El fichero '.strtolower($fichero).' es de base de datos</p>';
                break;
            default:
                echo '<p>El fichero '.$fichero.' no tiene extension</p>';
                break;
        }
    }