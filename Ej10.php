<!-- Enunciado: Escribir un programa creando una función que pasado un array de números nos diga la suma de todos ellos
Objetivo: Practicar con funciones y arrays. -->

<?php

    function suma(array $numeros){
        $suma = 0;
        foreach ($numeros as $valor) {
            $suma += $valor;
        }
        return $suma;
    }

    $numeros = [25, 1, 12, 45];
    //array_sum($numeros) es una funcion propia de php para hacer lo mismo
    echo "La suma de los números del array es: " . suma($numeros);