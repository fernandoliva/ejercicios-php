<!-- Ejercicio... [03]
Crear un código que pasado un número nos diga cuántas veces es necesario elevarlo para que pase de 10.000
Ej: si el número es 150, el resultado es 2 =>  1502 > 10.000
Ej: si el número es 5, el resultado es 6 => 56 > 10.000 -->

<?php
    $num = $_GET['num'];
    $elevate= 0;
    $result = 0;

    while ($result < 10000) {
        $elevate++;
        $result = $num ** $elevate;
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
    <p>Si el número es: <?php echo $num ?>. El resultado es <?php echo $result ?></p>
</body>
</html>