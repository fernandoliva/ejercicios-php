<!-- //Ejercicio up8
Escribir un programa que dado un array de palabras, te diga qué palabra es la más corta y cual la más larga.
$array = ["patata","cebolla","sal","pimienta","te","agua"];
Salida: La palabra más corta es ‘te’ y y la más larga es ‘pimienta’ -->

<?php 
    $array = ["patata","cebolla","sal","pimienta","te","agua"];
    $shortString = $array[0];
    $longString = $array[0];

    foreach ($array as $item) {
        if (strlen($item) < strlen($shortString)) {
            $shortString = $item;
        }
        if (strlen($item) > strlen($longString)) {
            $longString = $item;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>
<body>
    <p>La cadena corta es: <?php echo $shortString?></p>
    <p>La cadena larga es: <?php echo $longString?></p>
</body>
</html>