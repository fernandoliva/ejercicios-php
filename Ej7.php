<?php
    $paises = [
        "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels",
        "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris",
        "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin",
        "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam",
        "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm",
        "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius",
        "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest",
        "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw"
    ];
?>

<!DOCTYPE html>
<html lang="en">
    <style>
        body{
            font-family: Helvetica, sans-serif;
            background-color: #A1B0AB;
            color: #2D3047;
        }
        h1{
            text-align: center;
        }
        .card{
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: space-around;
            align-items: center;
        }
        .card-item{
            width: 20vw;
            height: 10vh;
            border: 0;
            display: flex;
            flex-direction: row;
            align-items: center;
            margin-bottom: 1rem;
            padding: 2rem;
            border-radius: 15px;
            background-color: #D5ECD4;
            font-size: 1.2rem;
            -webkit-box-shadow: 0px 0px 10px 0px rgba(107,96,84,0.7);
            box-shadow: 0px 0px 10px 0px rgba(107,96,84,0.7);
        }
    </style>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7 PHP</title>
</head>
<body>
    <h1>Listado de Paises y capitales</h1>
    <div class="card">
        <?php
            //ksort ordena por clave
            ksort($paises);
            // asort ordena por valor
            // asort($paises);
            foreach ($paises as $pais => $capital) {
                echo '<div class="card-item">';
                echo "<p>La capital de <b>".$pais."</b> es ".$capital."</p>";
                echo '</div>';
            }
        ?>
    </div>
</body>
</html>

