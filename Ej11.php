<!-- Enunciado: Crear una función que ‘milenialice’ un texto que se le pase, es decir:
    * los ‘que’ los convierte el ‘k’
    porque => xq
    * Pone todo en mayúsculas indistintamente
    * Gu/Bu => w
    * Igual => =
    * Sin apertura de ¡ ni ¿, ni acentuación
Podéis echarle imaginación ;
Objetivo: Practicar con funciones cadenas y arrays. Mover esas funciones a un fichero llamado util.php y hacer llamadas a dichas funciones desde otro fichero diferente. -->

<?php

    require './utils.php';
    $texto = "Buenas tardes, me da igual como se escribe, esto es un texto que esta escrito correctamente, porque... ¡aqui se respeta la ortografia!";
    echo milenizar($texto);