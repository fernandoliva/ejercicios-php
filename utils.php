<?php

function milenizar(string $texto){
    $texto = str_replace(
        ['que', 'porque', 'gu', 'bu', 'igual'], 
        ['k', 'xq', 'w', 'w', '='],
        strtolower($texto)
    );
    $finalText = eliminarTilde($texto);
    $finalText = ponerEnMayusculas($texto);
    return $finalText;
}

function eliminarTilde(string $texto){
    $texto = str_replace(
        ['á', 'é', 'í', 'ó', 'ú', 'ñ'], 
        ['a', 'e', 'i', 'o', 'u', 'n'],
        $texto
    );
    return $texto;
}

function ponerEnMayusculas($texto){
    for($character=0; $character<strlen($texto); $character++){
        if(floor(rand(0,1))){
            $texto[$character] = strtoupper($texto[$character]);
        }
    }

    return $texto;
}